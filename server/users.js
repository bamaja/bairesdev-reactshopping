import express from 'express';
import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

let router = express.Router();

function validateInput(data){
  let errors = {};

  if(Validator.isEmpty(data.username)){
    errors.user = 'This field is required';
  }
  if(Validator.isEmpty(data.password)){
    errors.password = 'This field is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}

router.post('/', (req, res) => {
    const { errors, isValid } = validateInput(req.body);

    if(isValid) {
      res.json({ success: true});
    }else{
      res.status(400).json(errors);
    }
})

export default router;
