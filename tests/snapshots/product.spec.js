import React from 'react';
import expect from 'expect';
import renderer from 'react-test-renderer';
import {render, shallow, mount} from 'enzyme';
import {Product} from '../../src/components/home/Product';
import {ProductList} from '../../src/components/home/ProductList';
import {ModalShow} from '../../src/components/home/ModalShow';
import {Login} from '../../src/pages/Login';
import {Header} from '../../src/components/header/Header';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';


function set_state(logged){
  const initial_state = {
    user: {
      actual: {username:'' , password: '', isLoggedin: logged },
      signed: []
    },
    products: {
      list: [],
      modal: {show: logged},
      cart: []
    }
  };
  return initial_state;
}

function mockItem(overrides) {
  return  {
             name: "Aldo",
             brand: "Baires",
             model: "86",
             desc: "Yes",
             sku: "123",
             price: "100"

         };
}
const prods =  {
  list: [  {
      "name": "Trustforge Auto Tag Mount",
      "brand": "Brions",
      "model": "X7",
      "sku": "13180a6d-86e0-408c-a5c0-28254bd97720",
      "price": 281,
      "desc": "Et sed quis eligendi non ut et sunt eveniet. Atque necessitatibus aliquam et soluta. Commodi quo molestiae dolor est repudiandae quos error asperiores. Fuga quod aperiam laborum dolores."
    },
    {
      "name": "Onenix Air Component",
      "brand": "Ambalt",
      "model": "OR-3186",
      "sku": "024f8ec0-193e-4897-8b3c-1b315691a3b5",
      "price": 126,
      "desc": "Harum aut autem magni earum. Tenetur quod tempore aut expedita. Itaque consequatur et laborum blanditiis eius magnam exercitationem. Nihil sequi sed minus ipsam sed. Rerum enim sint quisquam voluptas."
    }]};
  let store = configureStore()(set_state(false));
  const item = mockItem();

describe('Header', () => {
    it('Header User not logged in', () => {
      const wrapper = shallow(<Header {...set_state(false)}/>);
      console.log(wrapper.text());
      //expect(wrapper.find('Navbar').length).toBe(2);
      expect(wrapper.find('Nav').childAt(0).childAt(0).childAt(0).text()).toEqual('Signup');
      expect(wrapper.find('Nav').childAt(1).childAt(0).childAt(0).text()).toEqual('Login');
      });
      it('Header User logged in', () => {
        const wrapper = shallow(<Header {...set_state(true)}/>);
        expect(wrapper.find('Nav').childAt(0).childAt(0).childAt(0).text()).toEqual('Cart');
        expect(wrapper.find('Nav').childAt(1).childAt(0).text()).toEqual('Logout');
        });
    });

describe('Login', () => {
    it('Renders Form', () => {
      const wrapper = shallow(<Login user={set_state(false)}/>);
      expect(wrapper.find('h1').text()).toEqual('Login');
      expect(wrapper.find('Form').length).toBe(1);
      expect(wrapper.find('Checkbox').length).toBe(1);
      });
    });



describe('Product Item', () => {
  it('Renders Product Panel', () => {
    const wrapper = shallow(<Product product={item} store={store}/>);
    expect(wrapper.find('Panel').length).toBe(1);
    });
  it('Renders data', () => {
    const wrapper = shallow(<Product product={item} store={store}/>);
    expect(wrapper.find('h3').text()).toEqual('Aldo');
    expect(wrapper.find('p').text()).toEqual('86');
    expect(wrapper.find('Label').length).toBe(1);
    expect(wrapper.find('Button').length).toBe(1);
  })
  });


describe('Product List', () => {
  it('Renders List of 2 Products', () => {
    const wrapper = mount(<Provider store={store}><ProductList prods={prods.list} /></Provider>);
    expect(wrapper.find('Panel').length).toBe(2);
    });
  });

// describe('Modal', () => {
//   it('Modal Opens', () => {
//     const wrapper = mount(<Provider store={store}><ModalShow {...set_state(true)} /></Provider>);
//     //console.log(wrapper.children().text());
//     expect(wrapper.childAt(0).text()).toEqual('Aldo');
//     expect(wrapper.find('h4').text()).toEqual('86');
//     expect(wrapper.find('p').length).toBe(1);
//     expect(wrapper.find('Button').length).toBe(1);
//     const button = wrapper.find('Button').last();
//     expect(button.prop('bsStyle')).toBe('primary');
//     button.simulate('click', { stopPropagation: () => undefined });
//     });
//   });
