import _ from 'lodash';

export default function productsReducer(state = {}, action){
  let new_state = JSON.parse(JSON.stringify(state));
  switch(action.type){
    case 'LOADED_PRODUCTS':
      new_state.list = action.products;
      return new_state;

    case 'MODAL':
        new_state.modal = new_state.modal ? new_state.modal : {};
        new_state.modal = {
            show: true,
            name: action.name,
            brand: action.brand,
            model: action.model,
            price: action.price,
            desc: action.desc,
            sku: action.sku
        }
        return new_state;

    case 'MODAL_HIDE':
        new_state.modal = {
            show: false,
            name: '',
            brand: '',
            model: '',
            price: '',
            desc: '',
            sku: ''
        }
        return new_state;

    case 'ADD_PRODUCT':

        new_state.cart = new_state.cart ? new_state.cart : [];

        for(const index in new_state.list){

            if(new_state.list[index].sku === action.sku){
                new_state.cart.push(Object.assign({},new_state.list[index]));
                new_state.cart = _.sortBy(new_state.cart, (o) => { return o.name }) ;
                break;

            }
        }

        return new_state;

    case 'DELETE_ITEM':
          new_state.cart = new_state.cart ? new_state.cart : [];
          let index = _.findIndex(new_state.cart, ['sku', action.sku]);
          new_state.cart.splice(index, 1);

          return new_state;

    default:
        return state;
  }
}
