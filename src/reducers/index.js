import { combineReducers } from 'redux';
import user from './userReducer';
import products from './productsReducer';

const rootReducer = combineReducers({
  user: user,
  products: products
});

export default rootReducer;
