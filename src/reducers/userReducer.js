import _ from 'lodash';
export default function userReducer(state = [], action){
  let new_state = JSON.parse(JSON.stringify(state));
  switch(action.type){
    case 'LOGIN_USER':
        new_state.actual = {
          username:'' , password: '', isLoggedin: true
        };
      return new_state;
    case 'LOG_OUT':
      new_state.actual = {
        username:'' , password: '', isLoggedin: false
      };
      return new_state;
    case 'SIGN_UP':
      new_state.signed = new_state.signed ? new_state.signed : [];
      let index = _.findIndex(new_state.signed, ['username', action.user.username]);
          if(index >= 0){
              new_state.actual = (Object.assign({}, action.user));
              new_state.actual.isLoggedin = true;
              //new_state.cart = _.sortBy(new_state.cart, (o) => { return o.name }) ;
              break;

          }else{
            new_state.signed.push(Object.assign({}, action.user));
            new_state.actual = (Object.assign({}, action.user));
            new_state.actual.isLoggedin = true;
          }
      return new_state;



    default:
        return state;
  }
}
