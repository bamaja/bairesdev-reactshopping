import React from 'react';
import SignupForm from '../components/signup/SignupForm.js';
import { connect } from 'react-redux';
import {  Col, Button } from 'react-bootstrap';
import * as userActions from '../actions/userActions';

class Signup extends React.Component {

  render(){

    const { userSignUp } = this.props;

    return(
      <div className="row">
        <Col sm={2}>
        </Col>
        <Col sm={10}>
          <h1>Sign Up</h1>
        </Col>
        <SignupForm userSignUp={userSignUp}/>
      </div>
    );
  }
};


Signup.propTypes = {
  userSignUp: React.PropTypes.func.isRequired
}


function mapDispatchToProps(dispatch){
  return {
    userSignUp: user => dispatch(userActions.userSignUp(user))
  }
}

export default connect(null, mapDispatchToProps)(Signup);
