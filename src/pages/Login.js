import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import * as userActions from '../actions/userActions';
import _ from 'lodash';
import { Form, FormGroup, FormControl, ControlLabel, Checkbox, Col, Button } from 'react-bootstrap';

export class Login extends React.Component {

  constructor(props, context){
    super(props, context);

    this.state = {
      username: '',
      password: '',
      isLoggedin: false,
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e){
    this.setState({ [e.target.name]: e.target.value} );
  }

  onSubmit(e){
    e.preventDefault();
    this.setState({ errors: {} });
  //console.log(JSON.stringify(this.props.user));
    let index = _.findIndex(this.props.user.signed, ['username', this.state.username]);
    //console.log(index);
    if(index >= 0){
        this.props.loginUser(this.state);
        this.context.router.push('/');

    }else{
      this.setState({ errors: { user: "Invalid user." } });
    }

  }

  render(){
    const { errors } = this.state;
    return(
      <div className="row">
        <Col sm={2}>
        </Col>
        <Col sm={10}>
          <h1>Login</h1>
        </Col>

      <Form horizontal>
        <FormGroup controlId="formHorizontalEmail">
          <Col componentClass={ControlLabel} sm={2}>
            Username
          </Col>
          <Col sm={4}>
            <FormControl type="text" name="username" placeholder="Username" onChange={this.onChange} value={this.state.username} />
          </Col>
        </FormGroup>
           { errors.user && <div className="alert alert-danger" role="alert">{ errors.user}</div> }
        <FormGroup controlId="formHorizontalPassword">
          <Col componentClass={ControlLabel} sm={2}>
            Password
          </Col>
          <Col sm={4}>
            <FormControl type="password" name="password" placeholder="Password" onChange={this.onChange} value={this.state.password}/>
          </Col>
            { errors.password && <div className="card-panel red lighten-4">{ errors.password }</div> }
        </FormGroup>

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Checkbox>Remember me</Checkbox>
          </Col>
        </FormGroup>

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button type="submit"  onClick={this.onSubmit}>
              Log in
            </Button>
          </Col>
        </FormGroup>
      </Form>

      </div>

    );
  }
}

Login.propTypes = {
//  dispatch: React.PropTypes.func.isRequired,
  user: React.PropTypes.object.isRequired
};
Login.contextTypes = {
  router: React.PropTypes.object.isRequired
}

function mapStateToProps(state, ownProps){
  return {
    user: state.user
  };
}
function mapDispatchToProps(dispatch){
  return {
    loginUser: user => dispatch(userActions.loginUser(user))
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (Login);
