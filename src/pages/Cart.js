import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Table, Row,  Jumbotron, Button, Glyphicon } from 'react-bootstrap';

class Cart extends React.Component {

  constructor(props){
    super(props);
    this.deleteItem = this.deleteItem.bind(this);
  }
  deleteItem(e){
       const s = e.target.dataset.sku;
       console.log(s);
       this.props.dispatch({
           type: 'DELETE_ITEM',
           sku: s
       });
   }

  render(){
    let prods = this.props.prodcts;

    if(prods.length > 0){
    return(
      <Table responsive>
      <thead>
        <tr>
          <th>#</th>
          <th>Product</th>
          <th>Desc</th>
          <th>Model</th>
          <th>Price</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>

        {
         prods.map((pro, index) => {
           return(
          <tr key={index}>
            <td>{index+1}</td>
            <td>{pro.name}</td>
           <td>{pro.desc}</td>
           <td>{pro.model}</td>
           <td>{pro.price}</td>
           <td><Button bsSize="xsmall" data-sku={pro.sku}
                    onClick={this.deleteItem}>
                    Delete <Glyphicon glyph="remove-circle" />
                </Button></td>
          </tr>
         );
       }
       )}


      </tbody>
      </Table>




    );
    }else{
      return(
        <Row>
          <Jumbotron>
            <h2>No products in the Cart</h2>
            <p>Please add some products!</p>
          </Jumbotron>
          </Row>
      );
    }
  }
}

Cart.propTypes = {
  prodcts: React.PropTypes.array.isRequired
};

function mapStateToProps(state, ownProps){
  return ({
    prodcts: state.products.cart
  });
}

export default connect(mapStateToProps) (Cart);
