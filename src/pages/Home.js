import React from 'react';
import { connect } from 'react-redux';
import ProductList from '../components/home/ProductList';


class Home extends React.Component{

    render(){
      const products = this.props.products;
        return(
            <div>
                <ProductList prods={products.list} />
            </div>
        );
    }
}

Home.propTypes = {
  products: React.PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps){
  return ({
    products: state.products,
  });
}

export default connect(mapStateToProps) (Home);
