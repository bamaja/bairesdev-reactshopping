import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import rootReducer from './reducers/index';
import thunk from 'redux-thunk';
import { Router, browserHistory } from 'react-router';
import routes from './routes';
import { getProducts } from './actions/productActions';
import './css/style.css';

const initial_state = {
  user: {
    actual: {username:'' , password: '', isLoggedin: false },
    signed: []
  },
  products: {
    list: [],
    modal: {},
    cart: []
  }
}
const store = createStore(
  rootReducer,
  initial_state,
  compose(
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
);
store.dispatch(getProducts());

render(
  <Provider store={store}>
    <Router history={browserHistory} routes={routes} />
  </Provider>,
  document.getElementById('app')
);
