import productsApi from '../api/productsApi';

export function loadedProducts(products){
  return { type: 'LOADED_PRODUCTS', products};
}
//debugger;
export function getProducts() {
  return function (dispatch) {
    return productsApi.getAllProducts().then(products => {
      dispatch(loadedProducts(products));
    }).catch(error => {
      throw(error);
    });
  }
}
