import axios from 'axios';

export function loginUser(user){
  return { type: 'LOGIN_USER', user};
}

export function signinUser(user){
  return { type: 'SIGN_UP', user};
}

export function logout(){
  return { type: 'LOG_OUT' };
}

export function userSignUp(userData) {
  return dispatch => {
    return axios.post('/apiUsers', userData);
  }
}
