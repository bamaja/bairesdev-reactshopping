import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/app'
import Home from './pages/Home';
import Cart from './pages/Cart';
import Login from './pages/Login';
import Signup from './pages/Signup';
import LoggedInContainer from './components/login/loggedInContainer';


export default (
  <Route path="/" component={App}>
    <Route component={LoggedInContainer}>
      <IndexRoute component={Home} />
      <Route path="cart" component={Cart} />
    </Route>
    <Route path="login" component={Login} />
    <Route path="signup" component={Signup} />
  </Route>
);
