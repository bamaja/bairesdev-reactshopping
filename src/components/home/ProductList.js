import React from 'react';
import Product from './Product';
import ModalShow from './ModalShow';
import { connect } from 'react-redux';
import { Grid, Row, ProgressBar } from 'react-bootstrap';

export class ProductList extends React.Component {

  constructor(props, context){
       super(props, context);


   }
  //  componentWillMount(){
  //    if(!this.props.user.actual.isLoggedin){
  //        this.context.router.push('/');
  //    }
   //}

    render(){
      if(this.props.prods){
      return(
      <div>
      <Grid>
       <Row className="show-grid">

         {
          this.props.prods.map((product, index) =>
            <Product key={index} product={product}/>
          )}
      </Row>
      </Grid>
      <ModalShow />
      </div>
      );
    }else{

            return(
                <ProgressBar active now={100} />
            );

    }
  }
}
// ProductList.contextTypes = {
//   router: React.PropTypes.object.isRequired
// }
function mapStateToProps(state, ownProps){
  return {
    user: state.user
  };
}

export default connect(mapStateToProps) (ProductList);
