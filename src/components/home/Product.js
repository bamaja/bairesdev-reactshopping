
import React from 'react';
import { connect } from 'react-redux';
import { Panel, Col, Label, Button } from 'react-bootstrap';

export class Product extends React.Component {

  constructor(props){
       super(props);

       this.openModal = this.openModal.bind(this);
   }
   openModal(e){

        e.preventDefault();
        const n = e.target.dataset.name;
        const b = e.target.dataset.brand;
        const p = e.target.dataset.price;
        const d = e.target.dataset.desc;
        const m = e.target.dataset.model;
        const s = e.target.dataset.sku;

        this.props.dispatch({
            type: 'MODAL',
            name: n,
            brand: b,
            desc: d,
            price: p,
            model: m,
            sku: s
        });
    }

  render() {
    const prod = this.props.product;
     return (

       <Col xs={6} md={4} >
       <Panel>
        <h3 name="name">{prod.name}</h3>
        <h4 name="brand"> {prod.brand} </h4>
        <p name="model">{prod.model}</p>
        <h4 ><Label name="price">${prod.price}</Label></h4>
           <Button bsStyle="primary" onClick={this.openModal} data-name={prod.name} data-brand={prod.brand} data-model={prod.model}
             data-desc={prod.desc} data-price={prod.price} data-sku={prod.sku}>View</Button>
       </Panel>
     </Col>



      );
  }
}
Product.propTypes = {
  product: React.PropTypes.object.isRequired
}

export default connect() (Product);
