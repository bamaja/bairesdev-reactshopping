import React from 'react';
import { connect } from 'react-redux';
import { Modal, Button, Col, Label } from 'react-bootstrap';

export class ModalShow extends React.Component{

    constructor(props){
        super(props);
        this.modalHide = this.modalHide.bind(this);
        this.addToCart = this.addToCart.bind(this);
    }

    render()
    {
        const prod = this.props.modal;

        return(
          <Modal show={this.props.modal.show}>
               <Modal.Header>
                   <Modal.Title>
                       {prod.name}
                   </Modal.Title>
               </Modal.Header>

               <Modal.Body>
               <h4> {prod.brand} </h4>
               <p >{prod.desc}<br/>{prod.model}</p>
              <Label>${prod.price}</Label>
               </Modal.Body>
               <Modal.Footer>
                   <Button onClick={this.modalHide}>Close</Button>
                   <Button bsStyle="primary" onClick={this.addToCart}>Add</Button>
               </Modal.Footer>
           </Modal>
        )
    }

    addToCart(e){
        this.props.dispatch({
            type: 'ADD_PRODUCT',
            sku: this.props.modal.sku
        });
         this.props.dispatch({
            type: 'MODAL_HIDE'
        });
    }
    modalHide(e){
        this.props.dispatch({
            type: 'MODAL_HIDE'
        });
    }
}

ModalShow.propTypes = {
  modal: React.PropTypes.object.isRequired,
}

function mapStateToProps(state, ownProps){
  let modal;
      if(state.products.modal){
              modal = state.products.modal;

      }else{
              modal = {
                  show: false
              }
          }

      return {
              modal: modal
      }
}

export default connect(mapStateToProps)(ModalShow);
