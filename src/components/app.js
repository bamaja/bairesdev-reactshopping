import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import Header from './header/Header';

class App extends React.Component {
  render(){

    return(
      <div className="container-fluid">
      <div className="row">
        <Header />
      </div>
      <div className="container">
        {this.props.children}
      </div>
    </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.object.isRequired
};

export default App;
