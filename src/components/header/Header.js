import React, { PropTypes } from 'react';
import { Navbar,  Nav, NavItem, Glyphicon, Button } from 'react-bootstrap';
import { IndexLinkContainer, LinkContainer } from 'react-router-bootstrap';
import { Link, IndexLink } from 'react-router';
import { connect } from 'react-redux';

export class Header extends React.Component {

constructor(props, context){
     super(props, context);

     this.logout = this.logout.bind(this);
 }

  logout(){
    this.props.dispatch({
        type: 'LOG_OUT',
    });
    this.context.router.push('/login');
  }
  render(){
  return(
  <Navbar >

    <Navbar.Header>
      <Navbar.Brand>
          <IndexLinkContainer to="/">
            <NavItem>BairesDevShopping</NavItem>

            </IndexLinkContainer>
      </Navbar.Brand>
    </Navbar.Header>

    <Navbar.Collapse>
      <Nav pullRight>
        { !this.props.user.actual.isLoggedin ? (
        <LinkContainer to="/signup" activeClassName="active">
            <NavItem>
                Signup
            </NavItem>
        </LinkContainer>
      ): (
        <LinkContainer to="/cart">
            <NavItem>
                Cart
            </NavItem>
        </LinkContainer>
      )}
      { this.props.user.actual.isLoggedin ? (
        <NavItem onClick={this.logout}>
          Logout
        </NavItem>
      ) : (
        <LinkContainer to="/login" activeClassName="active">
            <NavItem>
                Login
            </NavItem>
        </LinkContainer>
       )}


      </Nav>
    </Navbar.Collapse>
  </Navbar>
);


};
}

Header.contextTypes = {
  router: React.PropTypes.object.isRequired
}

function mapStateToProps(state, ownProps){
  return ({
    user: state.user
  });
}

export default connect(mapStateToProps, null, null, {
  pure: false
})(Header);
