import React from 'react';
import { connect } from 'react-redux';
import { Router, browserHistory } from 'react-router';

class loggedInContainer extends React.Component {
  componentDidMount() {
    if (!this.props.isLoggedIn) {
      browserHistory.replace("/login")
    }
  }

  render() {
    if (this.props.isLoggedIn) {
      return this.props.children
    } else {
      return null
    }
  }
}


function mapStateToProps(state, ownProps) {
  return {
    isLoggedIn: state.user.actual.isLoggedin,
  }
}

export default connect(mapStateToProps)(loggedInContainer)
