import React from 'react';
import { Form, FormGroup, FormControl, ControlLabel, Checkbox, Col, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import _ from 'lodash';

class SignupForm extends React.Component {

  constructor(props, context){
    super(props, context);

    this.state = {
      username: '',
      password: '',
      isLoggedin: false,
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

  }

  onChange(e){
    this.setState({ [e.target.name]: e.target.value} );
  }
  onSubmit(e){
    e.preventDefault();
    this.setState({ errors: {} });
    // console.log(JSON.stringify(this.props.user));
    let index = _.findIndex(this.props.user.signed, ['username', this.state.username]);
    if(index < 0){
        this.props.userSignUp(this.state).then(
          () => {
              this.props.dispatch({
                  type: 'SIGN_UP',
                  user: this.state
              });
              this.context.router.push('/');
            },
          ({data}) => this.setState( { errors: data } )
        );
    }else{
      this.setState({ errors: { username: "User exists." } });
    }



  }

  render(){
    const { errors } = this.state;
    //console.log(JSON.stringify(errors));

    return(
      <Form horizontal>
        <FormGroup controlId="formHorizontalEmail">
          <Col componentClass={ControlLabel} sm={2}>
            Username
          </Col>
          <Col sm={4}>
            <FormControl type="text" name="username" placeholder="Username" onChange={this.onChange} value={this.state.username} />
          </Col>
        </FormGroup>
         { errors.username && <div className="alert alert-danger" role="alert">{ errors.username }</div> }

        <FormGroup controlId="formHorizontalPassword">
          <Col componentClass={ControlLabel} sm={2}>
            Password
          </Col>
          <Col sm={4}>
            <FormControl type="password" name="password" placeholder="Password" onChange={this.onChange} value={this.state.password}/>
          </Col>
        </FormGroup>
         { errors.password && <div className="alert alert-danger" role="alert">{ errors.password }</div> }

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button type="submit"  onClick={this.onSubmit}>
              Sign in
            </Button>
          </Col>
        </FormGroup>
      </Form>
   );
  }
}
SignupForm.propTypes = {
  userSignUp: React.PropTypes.func.isRequired
}
SignupForm.contextTypes = {
  router: React.PropTypes.object.isRequired
}

function mapStateToProps(state, ownProps){
  return {
    user: state.user
  };
}

export default connect(mapStateToProps) (SignupForm);
