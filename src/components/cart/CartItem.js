
import React from 'react';
import { connect } from 'react-redux';
import { Panel, Col, Label, Button } from 'react-bootstrap';

class CartItem extends React.Component {

  constructor(props){
       super(props);

       this.deleteItem = this.deleteItem.bind(this);
   }
   deleteItem(e){

        e.preventDefault();
        const s = e.target.dataset.sku;

        this.props.dispatch({
            type: 'DELETE_ITEM',
            sku: s
        });
    }

  render() {
    const prod = this.props.item;
     return (
       <tr>
         <td>{p}</td>
         <td>{prod.name}</td>
        <td>{prod.desc}</td>
        <td>{prod.model}</td>
        <td>{prod.price}</td>
        <td>{this.deleteIem}</td>
       </tr>
      );
  }
}
CartItem.propTypes = {
  item: React.PropTypes.object.isRequired
}

export default connect()(CartItem);
